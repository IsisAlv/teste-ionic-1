import { Input, Component, OnInit } from '@angular/core';
import 'rxjs/add/operator/map';

/**
 * Generated class for the MapComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'map',
  templateUrl: 'map.html'
})
export class MapComponent implements OnInit{

  @Input('lat') lat: number;
  @Input('lng') lng: number;

  app_id = 'LSBTL4xa7otJgYN41v0a';
  app_code = '4g11d05CNxlDBWPdzyatYg';

  platform: any;
  map: any;
  grupo: any;
  element: HTMLElement;
  behavior: any;
  defaultLayers: any;
  r_earth: number = 6378;

  constructor() {
  }

  ngOnInit(){
    console.log('Latitude: ', this.lat);
    console.log('Longitude: ', this.lng);

    let H = window['H'];

    this.platform = new H.service.Platform({
        app_id: this.app_id,
        app_code: this.app_code,
        useCIT: true,
        useHTTPS: true
    });
    this.grupo = new H.map.Group();

    this.element = document.getElementById('map');
    this.defaultLayers = this.platform.createDefaultLayers();

    this.map = new H.Map(this.element, this.defaultLayers.normal.map);
    this.behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(this.map));

    this.map.addObject(this.grupo);
  }

}
