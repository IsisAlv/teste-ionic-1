import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { PopoverController } from 'ionic-angular';
import { PopoverPage } from '../popover/popover';
import { TextToSpeech } from '@ionic-native/text-to-speech';

import { NovaPage } from '../nova/nova';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  v1 = "";
  v2 = "";

  operacao = 'soma';

  nova = NovaPage;

  constructor(public navCtrl: NavController, 
    private alertCtrl: AlertController,
    private popoverCtrl: PopoverController,
    private tts: TextToSpeech) {

  }

  /**
   * Função que testa a funcionalidade de alerta.
   */
  OnClick(){
    let alert = this.alertCtrl.create({
      title: 'Alerta Genérico',
      subTitle: 'O alerta funciona!',
      buttons: ['Bacana']
    });
    alert.present();
  }

  /**
   * Realiza soma dos valores.
   */
  clickSomar(){
    if(this.v2 == "" || this.v1 == "")
      this.alertaErro();
    else{
      let result: number;
      result = parseFloat(this.v1)+parseFloat(this.v2);

      this.alertaResultado(result);
    }
  }

  /**
   * Realiza subtração do primeiro valor pelo segundo.
   */
  clickSubtrair(){
    if(this.v2 == "" || this.v1 == "")
      this.alertaErro();
    else{
      let result: number;
      result = parseFloat(this.v1)-parseFloat(this.v2);

      this.alertaResultado(result);
    }
  }

  /**
   * Realiza divisão do primeiro valor pelo segundo.
   */
  clickDividir(){
    if(this.v2 == "" || this.v1 == "")
      this.alertaErro();
    else if(this.v1 == "0" && this.v2 == "0")
      this.alertaInvalido();
    else{
      let result: number;
      result = parseFloat(this.v1)/parseFloat(this.v2);

      this.alertaResultado(result);
    }
  }

  /**
   * Realiza multiplicação dos valores.
   */
  clickMultiplicar(){
    if(this.v2 == "" || this.v1 == "")
      this.alertaErro();
    else{
      let result: number;
      result = parseFloat(this.v1)*parseFloat(this.v2);

      this.alertaResultado(result);
    }
  }
  
  /**
   * Exibe em um alerta o resultado da operação matemática
   * @param result Resultado da operação matemática.
   */
  alertaResultado(result){
    let alert = this.alertCtrl.create({
      title: 'Resultado',
      subTitle: ''+result,
      buttons: ['Bacana']
    });
    alert.present();
    this.tts.speak(''+result)
    .then(() => console.log('Success'))
    .catch((reason: any) => console.log(reason));
  }

  /**
   * Exibe alerta de erro dos campos vazios.
   */
  alertaErro(){
    let alert = this.alertCtrl.create({
      title: 'Erro',
      subTitle: 'Campo(s) vazios! Favor preencher os campos',
      buttons: ['Tá...']
    });
    alert.present();
  }

  /**
   * Exibe alerta de erro da divisão 0/0.
   */
  alertaInvalido(){
    let alert = this.alertCtrl.create({
      title: 'Erro',
      subTitle: 'Sério?',
      buttons: ['UGH']
    });
    alert.present();
  }

  /**
   * Altera a operação sendo realizada.
   * @param operacao Nome da operação matemática.
   */
  setOperacao(operacao){
    this.operacao = operacao;
  }

  /**
   * Compara a operação dada com a que está selecionada.
   * Retorna true se forem iguais.
   * Do contrário, retorna false.
   * @param operacao Operação matemática a comparar com a selecionada.
   */
  verificaOperacao(operacao){
    if(this.operacao == operacao)
      return true;
    return false;
  }

  /**
   * Função que apresenta o popover na tela.
   * @param myEvent Evento que aciona a função.
   */
  presentPopover(myEvent) {
    let popover = this.popoverCtrl.create(PopoverPage);
    popover.present({
      ev: myEvent
    });
    popover.onWillDismiss(op => {
      if(op != undefined){
        this.operacao = op;
      }
    });
  }
}
