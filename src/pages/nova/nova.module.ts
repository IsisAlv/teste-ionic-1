import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NovaPage } from './nova';
import { MapComponent } from '../../components/map/map';

@NgModule({
  declarations: [
    NovaPage,
    MapComponent
  ],
  imports: [
    IonicPageModule.forChild(NovaPage),
  ],
})
export class NovaPageModule {}
