import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { BackgroundMode } from '@ionic-native/background-mode';

/**
 * Generated class for the NovaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-nova',
  templateUrl: 'nova.html',
})
export class NovaPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, private backgroundMode: BackgroundMode) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NovaPage');
  }

  onClickBGMode(){
    this.backgroundMode.enable();
    this.backgroundMode.moveToBackground();
  }

}
