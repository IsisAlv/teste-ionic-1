import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the PopoverPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-popover',
  templateUrl: 'popover.html',
})
export class PopoverPage {

  operacao: any;

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    public viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PopoverPage');
  }

  /**
   * Altera a operação matemática sendo realizada.
   * @param op Nome da operação matemática.
   */
  setOperacao(op){
    this.operacao = op;
    this.close();
  }

  /**
   * Fecha o popover e passa a operação selecionada como parâmetro.
   */
  close(){
    this.viewCtrl.dismiss(this.operacao);
  }
}
