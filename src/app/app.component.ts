import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { TabsPage } from '../pages/tabs/tabs';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { BackgroundMode } from '@ionic-native/background-mode';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = TabsPage;

  constructor(platform: Platform, statusBar: StatusBar, 
    splashScreen: SplashScreen, 
    localNtf: LocalNotifications,
    private backgroundMode: BackgroundMode) {
    platform.ready().then(() => {
      backgroundMode.enable();
      if(backgroundMode.isEnabled())
        console.log('Background mode enabled');
      
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      setTimeout(function(){splashScreen.hide();},300);
    });
    let data = new Date();
    data.setHours(16,20,0);
    localNtf.schedule({
      id:1,
      text: "Notificação Teste",
      trigger: {at: data}
    });
  }
}
